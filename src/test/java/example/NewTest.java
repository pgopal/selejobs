package example;		

//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class NewTest {
	    //private WebDriver driver;
        Capabilities chromeCapabilities = DesiredCapabilities.chrome();
        Capabilities firefoxCapabilities = DesiredCapabilities.firefox();
        private RemoteWebDriver chrome;
        private RemoteWebDriver firefox;
		@Test
		public void testEasy() throws MalformedURLException {
			//System.setProperty("webdriver.gecko.driver", "./geckodriver.exe");
			//WebDriver driver = new FirefoxDriver();
			//driver.get("https://edition.cnn.com/");
			//String title = driver.getTitle();
			//Assert.assertTrue(title.contains("CNN"));

			RemoteWebDriver chrome = new RemoteWebDriver(new URL("http://3.87.161.96:4444/wd/hub"), chromeCapabilities);
		    RemoteWebDriver firefox = new RemoteWebDriver(new URL("http://3.87.161.96:4444/wd/hub"), firefoxCapabilities);
		    // run against chrome
		    chrome.get("https://www.google.com");
		    System.out.println(chrome.getTitle());
		    // run against firefox
		    firefox.get("https://www.google.com");
		    System.out.println(firefox.getTitle());
		    chrome.quit();
		    firefox.quit();
		}
		@BeforeTest
		public void beforeTest() {	
		    //driver = new FirefoxDriver();  

		}		
		@AfterTest
		public void afterTest() {
			//driver.quit();
		}		
}
